package game;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

public class SpriteSheet {

	public String path; //Location of the image file
	//The size of the image, number of sprites per row and column, and the size in pixels
	//of each sprite
	public int width, height, rows, cols, size;
	
	//The buffered image
	public BufferedImage image;
	
	public SpriteSheet(String path, int size){
		try {
			image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
		} catch (IOException e) {
			return;
		}
		this.size = size;
		this.path = path;
		this.width = image.getWidth();
		this.height = image.getHeight();
		cols = width / size;
		rows = height / size;
	}
	
	//Gets the byte buffer of the image
	public ByteBuffer GetBuffer(BufferedImage image){
		int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
		
		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 3);
		
		for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
                buffer.put((byte) (pixel & 0xFF));               // Blue component
                //buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
            }
        }

        buffer.flip();
		
		return buffer;
	}
	
	//Gets the byte buffer of the subImage at a particular location of the spritesheet
	public ByteBuffer GetBuffer(BufferedImage image, int index){
		return GetBuffer(image.getSubimage((index % width) * size, (index / width) * size, size, size));
	}
	
	public ByteBuffer GetBuffer(){
		return GetBuffer(image);
	}
	public ByteBuffer GetBuffer(int index){
		return GetBuffer(image, index);
	}
}