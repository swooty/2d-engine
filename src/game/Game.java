package game;

import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import input.*;
import physics.*;

public class Game implements Runnable {
	
	public static final int ticksPerSecond = 60;
	
	//Attributes for the game window
	static final int WIDTH = 720, HEIGHT = WIDTH * 9 / 16;
	static final String NAME = "Game";

	// The window handle
	private long window;
	
	GLFWKeyCallback keyCallback;

	public void run() {
		try {
			init();
			loop();

			// Free the window callbacks and destroy the window
			glfwFreeCallbacks(window);
			glfwDestroyWindow(window);
		} finally {
			// Terminate GLFW and free the error callback
			glfwTerminate();
			glfwSetErrorCallback(null).free();
		}
	}

	private void init() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable


		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, NAME, NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, keyCallback = new InputHandler());

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		// Center our window
		glfwSetWindowPos(
			window,
			(vidmode.width() - WIDTH) / 2,
			(vidmode.height() - HEIGHT) / 2
		);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);
		
		InputHandler.init();
	}

	private void loop() {
		GL.createCapabilities();

		// Set the clear color
		glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

		//Used to store the time since last run
		long initTime = System.nanoTime();
		//The number of nanoseconds per tick
		double nanoPerTick = 1000000000.0 / ticksPerSecond;
		
		double delta = 0;
		while ( !glfwWindowShouldClose(window) ) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

			glfwSwapBuffers(window); // swap the color buffers

			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();
			
			long now = System.nanoTime();
			//Adds the number of ticks to do since the last update
			delta += (now - initTime) / nanoPerTick;
			initTime = now;		
			//Loops for all ticks to catch up on
			while (delta >= 1){
				tick();
				delta--;
			}
		}
	}

	public static void main(String[] args) {
		new Game().run();
	}
	
	//This is the function that runs 60 times per second
	//Used to invoke ticks for input, physics, and game logic
	private void tick(){
		InputHandler.tick();
		Physics.tick();
	}
}