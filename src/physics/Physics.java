package physics;

import map.*;
import game.*;

import java.util.ArrayList;
import java.util.Collections;

import entity.*;

public class Physics {
	
	public static int TICK = Game.ticksPerSecond;
	
	public static ArrayList<BlockEntity> blocks;
	public static ArrayList<MoveableEntity> entities;
	
	public static void init(){
		//Initializes the entities list
		entities = new ArrayList<MoveableEntity>();
		
		
		blocks = new ArrayList<BlockEntity>();
		//Adds all blocks from the map to the block entities
		for(int x = 0; x < Level.CURRENT.width; x++){
			for(int y = 0; y < Level.CURRENT.height; x++){
				blocks.add(new BlockEntity(Block.BLOCKS[Level.CURRENT.blocks[x][y]], x,y));
			}
		}
	}
	
	public static void tick(){
		//Iterates through the list of entities, updating them
		for(MoveableEntity e : entities){
			updateEntity(e);
		}
	}
	
	static void updateEntity(MoveableEntity e){
		//Initializes grounded to false unless again proven true
		e.grounded = false;
		//Comparator is used to sort other entities by proximity
		//to this entity
		MoveableComparator c = new MoveableComparator(e);
		
		ArrayList<Entity> sorted = new ArrayList<Entity>(entities);
		sorted.addAll(blocks);
		sorted.remove(e);
		Collections.sort(sorted, c);
		//Sorted contains all other entities sorted
		float drag = 1;
		for(Entity o : sorted){
			if (o != e){ //Not the same entity
				
				//Collision should set grounded to true if grounded
				//And vX or vY to 0 if collided
				if (!e.Collision(o)){//Performs the collision step
					//Didn't collide, we can stop testing for collision now
					break;
				}else{
					//If it collided with something, we can change drag
					//from 1 to the drag coefficient
					if (drag == 1){
						drag = e.drag;
					}
				}
			}
		}
		//If it wasn't grounded after all that, it can decrement velocity
		//for gravity
		if (!e.grounded){
			e.vY -= e.gravity / TICK;
		}
		
		//Moves position by velocity, if it collided, the proper velocity
		//should be 0
		//If it didn't collide, drag will be 1, otherwise, drag will be the
		//drag coefficient
		e.x += e.vX * drag / TICK;
		e.y += e.vY * drag / TICK;
	}
}
