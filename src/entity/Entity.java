package entity;

public abstract class Entity {
	//Position
	public float x, y;
	
	//Collision width and height
	public final float width, height;
	
	public Entity(float width, float height){
		this.width = width;
		this.height = height;
	}
	
	public void Spawn(float x, float y){
		this.x = x;
		this.y = y;
	}
}
