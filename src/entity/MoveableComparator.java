package entity;

import java.util.Comparator;

/*Comparator class for the MoveableEntity, depending on the distance from other
 * object
 */
public class MoveableComparator implements Comparator<Entity> {
	
	public MoveableEntity e;
	
	public MoveableComparator(MoveableEntity e){
		this.e = e;
	}
	
	@Override
	public int compare(Entity o1, Entity o2) {
		float x1 = X(e, o1), x2 = X(e, o2), y1 = Y(e, o1), y2 = Y(e, o2);
		double a = Math.sqrt((x1 * x1) + (y1 * y1));
		double b = Math.sqrt((x2 * x2) + (y2 * y2));
		if (a < b){
			return -1;
		}else if (a > b){
			return 1;
		}else{
			return 0;
		}
	}
	
	float X(Entity a, Entity b){
		return Math.abs(a.x - b.x - (a.width + b.width));
	}
	float Y(Entity a, Entity b){
		return Math.abs(a.y - b.y - (a.height + b.height));
	}

}
