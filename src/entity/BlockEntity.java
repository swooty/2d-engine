package entity;

import map.Block;

public class BlockEntity extends Entity {
	
	public final Block block;

	public BlockEntity(Block block, int x, int y) {
		super(1,1);
		this.block = block;
		this.x = x + 0.5f;
		this.y = y + 0.5f;
	}

}
