package entity;

import physics.*;

public class MoveableEntity extends Entity{
	
	public final float gravity, maxFall, drag;
	
	public float vX, vY;
	public boolean grounded;

	public MoveableEntity(float width, float height, float gravity, float maxFall, float drag) {
		super(width, height);
		this.gravity = gravity;
		this.maxFall = maxFall;
		this.drag = drag;
	}
	
	public boolean Collision(Entity other){
		//Uses a bounding box collision formula, this is just an algebraically
		//simplified version of comparing positions and widths/heights divided
		//by 2
		boolean colliding = (Math.abs(x - other.x) * 2 < (width + other.width)) &&
		         (Math.abs(y - other.y) * 2 < (height + other.height));
		
		if (colliding){
			//xSign and ySign will be 1 or -1, depending if the entity
			//is to the left/right or top/bottom of the other entity,
			//respectively
			float xSign = x - other.x;
			xSign /= Math.abs(xSign);
			float ySign = y - other.y;
			ySign /= Math.abs(ySign);
			
			//newX and newY represent the new x or y position of the entity
			//if they're moved to not intersect with the object
			float newX = other.x + xSign * other.width / 2f + xSign * width / 2f;
			float newY = other.y + ySign * other.height / 2f + ySign * height / 2f;
			
			//Compares the differences between the current x and newX, and the
			//current y and newY, whichever is less of a difference is used
			//The velocity of the selected coordinate is set to 0, this is
			//important for multiple collisions in the same physics frame
			if (Math.abs(y - newY) <= Math.abs(x - newX)){
				y = newY;
				vY = 0;
				if (ySign > 0){
					grounded = true;
				}
			}else{
				x = newX;
				vX = 0;
			}
		}
		return colliding;
	}

}
