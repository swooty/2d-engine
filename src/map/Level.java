package map;

import color.*;

public class Level {
	public static Level CURRENT;
	
	public int[][] blocks; //2D array of indices to store block locations
	public int width, height;
	
	public Color backgroundColor;
	
	public Level(int width, int height, Color backgroundColor){
		this.width = width;
		this.height = height;
		this.backgroundColor = backgroundColor;
		
		blocks = new int[width][height];
		
		//Sets all blocks to -1 using the place function
		//-1 represents an empty space
		Place(0,0,width-1,height-1, -1);
	}
	
	public Level(int width, int height){
		this(width, height, Color.BLACK);
	}
	
	//Places a block in the location
	//Also checks for out of bounds
	public void Place(int x, int y, int block){
		if (x < 0 || y < 0 || x >= width || y >= height){
			return;
		}
		blocks[x][y] = block;
	}
	
	//Places a square of one block, specified with two corners
	public void Place(int startX, int startY, int endX, int endY, int block){
		//Swaps startX and endX, and startY and endY if the start values are higher
		//than the end values, this allows for the for loops to work properly
		if (startX > endX){
			int t = startX;
			startX = endX;
			endX = t;
		}
		if (startY > endY){
			int t = startY;
			startY = endY;
			endY = t;
		}
		
		//Calls the place function for all blocks in between the two specified
		for (int x = startX; x <= endX; x++){
			for (int y = startY; y <= endY; y++){
				Place(x,y,block);
			}
		}
	}
	
	//Getter
	public int Get(int x, int y){
		return blocks[x][y];
	}
}
