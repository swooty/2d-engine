package map;

public class Block{
	
	public enum Type {Pass, Block, Kill} //Collision behavior
	//Pass - No behavior, no collision
	//Block - Simple level block, collision
	//Kill - Kills
	public String name;
	public int[] sprites; //Contains the sprite sheet indices for the animation
	//Each index represents a sprite in the spritesheet, the array contains all
	//sprites in the animation
	
	public Type type;
	
	//4 constructors, allowing sprites to be filled by a single sprite or multiple
	//and allowing type to not be specified (It's set to Block by default)
	public Block(String name, int[] sprites, Type type){
		this.name = name;
		this.sprites = sprites;
		this.type = type;
	}
	
	public Block(String name, int[] sprites){
		this(name, sprites, Type.Block);
	}
	
	public Block(String name, int sprite, Type type){
		this(name, new int[]{sprite}, type);
	}
	
	public Block(String name, int sprite){
		this(name, new int[]{sprite}, Type.Block);
	}
	
	//Contains the static, master list of all blocks in the game
	//The level contains indices to access these
	//The level editor will use this for the list of blocks to place
	public static Block[] BLOCKS = {
			new Block("Stone", 0),
			new Block("Water", 1, Type.Pass),
			new Block("Lava", 2, Type.Kill)
	};
}
