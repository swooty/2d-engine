package input;

import org.lwjgl.glfw.GLFWKeyCallback;
import static org.lwjgl.glfw.GLFW.*;

public class InputHandler extends GLFWKeyCallback {
	public static Controller[] controllers = new Controller[4];
	
	public static boolean[] keys = new boolean[65536];

    // The GLFWKeyCallback class is an abstract method that
    // can't be instantiated by itself and must instead be extended
    // 
    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        // TODO Auto-generated method stub
        keys[key] = action != GLFW_RELEASE;
    }

    // boolean method that returns true if a given key
    // is pressed.
    public static boolean isKeyDown(int keycode) {
        return keys[keycode];
    }
    
    //Initializes the 4 controllers
    //Temporarily sets up 0 for keyboard, and the rest to null
    public static void init(){
    	controllers[0] = new Controller();
    	for(int i = 1; i < controllers.length; i++){
    		controllers[i] = null;
    	}
    }
    
    //Input tick, 60 times per second
    public static void tick(){
    	Controller c = controllers[0];
    	//Sets the movement axis, -1, 0, or 1 for both horizontal and vertical
    	c.hor = 0;
    	c.vert = 0;
    	if (isKeyDown(GLFW_KEY_LEFT)){
			c.hor--;
		}
    	if (isKeyDown(GLFW_KEY_RIGHT)){
			c.hor++;
		}
    	if (isKeyDown(GLFW_KEY_DOWN)){
			c.vert--;
		}
    	if (isKeyDown(GLFW_KEY_UP)){
			c.vert++;
		}
    	
    	
    	boolean lastJump = c.jump, lastAction = c.action;
    	c.jump = isKeyDown(GLFW_KEY_Z);
    	c.jumpDown = !lastJump && c.jump;
    	c.jumpUp = lastJump && !c.jump;
    	
    	c.action = isKeyDown(GLFW_KEY_X);
    	c.actionDown = !lastAction && c.action;
    	c.actionUp = lastAction && !c.action;
    }
}