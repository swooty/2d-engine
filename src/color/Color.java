package color;

public class Color {
	public static Color RED = new Color(1,0,0,1), GREEN = new Color(0,1,0,1), BLUE = new Color(0,0,1,1),
			BLACK = new Color(0,0,0,1), WHITE = new Color(1,1,1,1), YELLOW = new Color(1,1,0,1),
			CYAN = new Color(0,1,1,1), VIOLET = new Color(1,0,1,1);
	
	public float r, g, b, a;
	
	public Color(float r, float g, float b){
		this(r,g,b,1);
	}
	public Color(float r, float g, float b, float a){
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
}